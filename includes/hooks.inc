<?php

/**
 * @file
 * Hook implementations
 */

/**
 * Implements hook_menu().
 */
function everyscape_menu() {
  $items = array();

  $items['admin/config/everyscape'] = array(
      'title' => 'EveryScape',
      'description' => 'Administer EveryScape module settings',
      'position' => 'right',
      'weight' => 0,
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('administer everyscape module settings'),
  );

  $items['admin/config/everyscape/settings'] = array(
      'title' => 'EveryScape Settings',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('everyscape_admin_settings_form'),
      'access arguments' => array('administer everyscape module settings'),
      'file' => 'includes/admin.inc',
      'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function everyscape_block_info() {
  $num = variable_get('everyscape_number_of_blocks', 1);
  for ($i = 0; $i < $num; $i++) {
    $blocks['everyscape_panorama_' . $i] = array(
        'info' => t('Everyscape Panorama #' . ($i + 1)),
        'cache' => DRUPAL_NO_CACHE,
        'properties' => array('administrative' => TRUE),
    );
  }

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function everyscape_block_configure($delta) {
  if (preg_match('/everyscape_panorama_/', $delta)) {
    $form['panorama_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Panorama ID'),
        '#default_value' => variable_get($delta . '_panorama_id'),
    );
    $form['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => variable_get($delta . '_width', variable_get('everyscape_default_width')),
    );
    $form['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => variable_get($delta . '_height', variable_get('everyscape_default_height')),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function everyscape_block_save($delta = '', $edit = array()) {
  if (preg_match('/everyscape_panorama_/', $delta)) {
    variable_set($delta . '_panorama_id', $edit['panorama_id']);
    variable_set($delta . '_width', $edit['width']);
    variable_set($delta . '_height', $edit['height']);
  }
  return;
}

/**
 * Implements hook_block_view().
 */
function everyscape_block_view($delta = '') {
  if (preg_match('/everyscape_panorama_/', $delta)) {
    $path = drupal_get_path('module', 'everyscape');

    $settings = array(
        array(
            'id' => 'everyscape-player-' . $delta,
            'poiID' => variable_get($delta . '_panorama_id'),
        ),
    );
    drupal_add_js("http://api.everyscape.com/api/api.js?modules=Script&v=1.x&key=AQAAAAAAAAAxVFlxckiyz00y6Kd0c68wQwQAAAAAAADr1twxAAAAAA&modules=Script", 'external');
    drupal_add_js(array('everyscapePlayers' => $settings), 'setting');
    drupal_add_js("$path/js/everyscape.js");

    $output = theme('everyscape_block', array('delta' => $delta, 'width' => variable_get($delta . '_width'), 'height' => variable_get($delta . '_height')));

    return array('content' => $output);
  }
  return;
}

/**
 * Implements hook_theme().
 */
function everyscape_theme() {
  $items = array();

  $items['everyscape_block'] = array(
      'variables' => array('delta' => NULL, 'width' => NULL, 'height' => NULL),
      'file' => 'includes/theme.inc',
  );

  return $items;
}