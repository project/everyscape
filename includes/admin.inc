<?php

/**
 * @file
 * Administrative forms and pages
 */

function everyscape_admin_settings_form() {

  $form['everyscape_number_of_blocks'] = array(
        '#type' => 'textfield',
        '#title' => t('Number of Blocks to Enable'),
        '#default_value' => variable_get('everyscape_number_of_blocks'),
    );
    $form['everyscape_default_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Default Block Width'),
        '#default_value' => variable_get('everyscape_default_width', '100%'),
    );
    $form['everyscape_default_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Default Block Height'),
        '#default_value' => variable_get('everyscape_default_height', '300px'),
    );

  return system_settings_form($form);
}