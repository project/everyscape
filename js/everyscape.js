(function ($) {
  
  function getQueryString(ji)
  {
    hu = window.location.search.substring(1);
    gy = hu.split("&");
    for (i=0;i<gy.length;i++) {
      ft = gy[i].split("=");
      if (ft[0] == ji) {
        return ft[1];
      }
    }
  }

  Drupal.behaviors.everyscape = {
    attach: function(context) {
      for(var i in Drupal.settings.everyscapePlayers) {
        var container = $('#' + Drupal.settings.everyscapePlayers[i].id);
        if (container.children().length == 0) {
          var viewer = EveryScape.installViewer({
            container:Drupal.settings.everyscapePlayers[i].id,
            type:"FULL"
          }, {
            panorama:{
              poiId:Drupal.settings.everyscapePlayers[i].poiID
            }
          }, {});
        }
      }
    }
  } 

})(jQuery);