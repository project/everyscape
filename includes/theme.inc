<?php

function theme_everyscape_block(&$variables) {
  $delta = $variables['delta'];
  $width = $variables['width'];
  $height = $variables['height'];

  $output = '';
  $output .= sprintf('<div id="everyscape-player-%s" style="width: %s; height: %s;"></div>', $delta, $width, $height);
  return $output;
}